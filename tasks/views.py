from projects.models import Project
from tasks.models import Task
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm


# Create your views here.
@login_required
def show_project(request, id):
    tasks = get_object_or_404(Project, id=id)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/tasks.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = TaskForm()
        context = {"form": form}

    return render(request, "tasks/create_task.html", context)


@login_required
def assignee_list(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {
        "task_list": task_list,
    }
    return render(request, "tasks/my_tasks.html", context)
